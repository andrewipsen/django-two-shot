from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def get_receipts(request):
    # GET all the instances of Receipt
    list = Receipt.objects.filter(purchaser=request.user)

    # add that data to the context boiii
    context = {"receipt_instances": list}

    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipts(request):
    if request.method == "POST":
        # use form to create the formpage
        form = ReceiptForm(request.POST)
        # validate that bih
        if form.is_valid:
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()

    context = {"expense_instances": list, "receipts": receipts}

    return render(request, "receipts/categories.html", context)


@login_required
def category_create(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid:
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
    context = {"form": form}
    return render(request, "receipts/category_create.html", context)


@login_required
def account_list(request):
    list = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()

    context = {"account_data": list, "receipts": receipts}
    return render(request, "receipts/accounts.html", context)


@login_required
def account_create(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/account_create.html", context)
