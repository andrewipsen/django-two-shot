from django.urls import path
from receipts.views import (
    get_receipts,
    create_receipts,
    category_list,
    account_list,
    category_create,
    account_create,
)

urlpatterns = [
    path("", get_receipts, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", category_create, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", account_create, name="create_account"),
]
