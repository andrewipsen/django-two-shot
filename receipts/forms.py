# import any models you'll use as well as the ModelForm
from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        # define which model we're using
        model = Receipt
        fields = ("vendor", "total", "tax", "date", "category", "account")


class ExpenseForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
