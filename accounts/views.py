from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignUpForm


# write a view that uses a GET to display and logs them in
# with POST
def user_login(request):
    # if the request is for a post
    if request.method == "POST":
        # the form will use the form we created
        form = LoginForm(request.POST)
        # if it's all dandy
        if form.is_valid():
            # define variables with the validated data
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            # create variable for authenticated data
            user = authenticate(request, username=username, password=password)

            # if the variable is authenticated
            if user is not None:
                # login that user
                login(request, user)
                # redirect to home
                return redirect("home")
    else:
        # if it's not a post, the request does not fill the form parameter
        form = LoginForm()
    # add the form to our context!
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    # log out the client that request it
    logout(request)
    return redirect("login")


def user_signup(request):
    # if the request is for a post
    if request.method == "POST":
        # we'll use the signupform for the client's request
        form = SignUpForm(request.POST)
        # check if data was filled in valid methinks?
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            # if these match
            if password == password_confirmation:
                # create variable user that takes the above objects
                user = User.objects.create_user(username, password=password)
                # login that user and request
                login(request, user)
                # send them back to the lobby
                return redirect("home")
            # if the passwords don't match
            else:
                form.add_error("password", "passwords do not match")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
